function lessonOne(){
	console.clear();
	for (var i = 10; i <= 20; i++){
		console.log(i);
	}
	alert("Вывод значений - в консоли!");
}

function lessonOneOne(){
	console.clear();
	for (var i = 10; i <= 20; i++){
		console.log(Math.pow(i,2));
	}
	alert("Вывод значений - в консоли!");
}

function lessonOneTwo(){
	console.clear();
	
	var summ = 0;
	for (var i = 10; i <= 20; i++){
		console.log(summ += i);
	}
	alert("Вывод значений - в консоли!");
}

function getValue(firstID, secondID, resultID, isBool = true){
	console.clear();
	if(isBool){
		document.getElementById(resultID).innerHTML = '';
	}
	
	var x1 = document.getElementById(firstID).value;
	var x2 = document.getElementById(secondID).value;
	var resultDiv = document.getElementById(resultID);
	var parsX1 = parseInt(x1);
	var parsX2 = parseInt(x2);
	var summ = 0;
	var multiply = 1;
	var simple = "";
	
	return [parsX1, parsX2, resultDiv, x1, x2, summ, multiply, simple];
}

function isInputInvalid (x1, x2, parsX1, parsX2, secondCheck = true, firstCheck = true){
	if (firstCheck){
		if (x1.length === 0 || x2.length === 0){
			alert("Поля x1 и x2 должны быть заполнены.");
			return true;
		}
	}
	
	if (Number.isNaN(parsX1) || Number.isNaN(parsX2)){
		alert("В поля х1 и х2 должны быть введены числовые значения.");
		return true;
	}
	
	if (secondCheck){
		if (parsX1 > parsX2){
			alert("Значение х1 должно быть меньше х2.");
			return true;
		}
	}
	
	return false;
}

function buttonClick(){
	[parsX1, parsX2, resultDiv, x1, x2] = getValue('x1', 'x2', 'result', false);
	
	if(isInputInvalid (x1, x2, parsX1, parsX2, false, false)) return;
	
	resultDiv.append("x1 + x2 = " + (parsX1 + parsX2));
}

function buttonClickOne(){
	[parsX1, parsX2, resultDiv, x1, x2] = getValue('firstValueOne', 'secondValueOne', 'resultOne');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2, false, false)) return;
	
	resultDiv.append("x1 + x2 = " + (parsX1 + parsX2));		
}

function buttonClickTwo(){
	[parsX1, parsX2, resultDiv, x1, x2] = getValue('firstValueTwo', 'secondValueTwo', 'resultTwo');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2, false)) return;
	
	resultDiv.append("x1 + x2 = " + (parsX1 + parsX2));		
}

function buttonClickThree(){
	[parsX1, parsX2, resultDiv, x1, x2, summ] = getValue('firstValueThree', 'secondValueThree', 'resultThree');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2)) return;
	
	for(var i = parsX1; i <= parsX2; i++){
		summ += i;
	}
	resultDiv.append("Сумма всех Х = " + summ);		
	
}

function buttonClickFour(){
	[parsX1, parsX2, resultDiv, x1, x2, summ] = getValue('firstValueFour', 'secondValueFour', 'resultFour');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2)) return;
	
	for(var i = parsX1; i <= parsX2; i++){
		summ += i;
	}
	resultDiv.append("Сумма всех Х = " + summ);		
}

function buttonClickFive(){
	[parsX1, parsX2, resultDiv, x1, x2, summ, multiply] = getValue('firstValueFive', 'secondValueFive', 'resultFive');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2)) return;
	
	var r1 = document.getElementById('rFive1');
	
	if(r1.checked){
		for(var i = parsX1; i <= parsX2; i++){
			summ += i;
		}
		resultDiv.append("Сумма всех Х = " + summ);	
	}
	else{
		for(var i = parsX1; i <= parsX2; i++){
			multiply *= i;
		}
		resultDiv.append("Произведение всех Х = " + multiply);	
	}
}

function buttonClickSix(){
	[parsX1, parsX2, resultDiv, x1, x2, summ, multiply, simple] = getValue('firstValueSix', 'secondValueSix', 'resultSix');
	
	if(isInputInvalid (x1, x2, parsX1, parsX2)) return;
	
	var r1 = document.getElementById('rSix1');
	var r2 = document.getElementById('rSix2');
	
	if(r1.checked){
		for(var i = parsX1; i <= parsX2; i++){
			summ += i;
		}
		resultDiv.append("Сумма всех Х = " + summ);	
	}
	else if(r2.checked){
		for(var i = parsX1; i <= parsX2; i++){
			multiply *= i;
		}
		resultDiv.append("Произведение всех Х = " + multiply);	
	}
	else{
		if (parsX1 < 2 || parsX2 < 2) {
			document.getElementById('resultSix').innerHTML = '';
			alert("Значения х1 и х2 должны быть больше 1.");
			var x1 = document.getElementById('firstValueSix').value = '';
			var x2 = document.getElementById('secondValueSix').value = '';
			return;
		}
		for (var i = parsX1; i <= parsX2; i++) {
			var isSimple = true;
			for (var j = 2; j < i; j++) {
				if (i % j == 0) {
					isSimple = false;
					break;
				}
			}
			if (isSimple) {
				simple += i + "; ";
			}
		}
		resultDiv.append("Перечень простых чисел: " + simple);
	}
	
}

function buttonClear(firstID, secondID){
	console.clear();
	
	var x1 = document.getElementById(firstID).value = '';
	var x2 = document.getElementById(secondID).value = '';
}
