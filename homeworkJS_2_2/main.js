// массив записей
var data = (localStorage.getItem('todoList'))
  ? JSON.parse(localStorage.getItem('todoList'))
  : [];
var completeData = (localStorage.getItem('completeList'))
    ? JSON.parse(localStorage.getItem('completeList'))
    : [];

// синхронизация localStorage и Массива JS
function storageSync() {
  localStorage.setItem('todoList', JSON.stringify(data));
  localStorage.setItem('completeList', JSON.stringify(completeData));
}

// рисуем начальный список
renderTodoList();
renderCompleteList();

// Добавить новый элемент в список
document.getElementById('btn-create').addEventListener('click', function () {
  var value = document.getElementById('input-create').value;
  if (value) {
    addItemToDOM(value);
    document.getElementById('input-create').value = '';

    data.push(value);
    storageSync();
  } 
});

// Очистить фильтр
document.getElementById('clearFilter').addEventListener('click', function () {
    var filter = document.getElementById('filter').value = "";
    elementFiltering();
});

function renderTodoList() {
  if (!data.length) return;

  for (var i = 0; i < data.length; i++) {
    var value = data[i];
    addItemToDOM(value);
  }
}

function renderCompleteList() {
    if (!completeData.length) return;

    for (var i = 0; i < completeData.length; i++) {
        var value = completeData[i];
        addItemToCompleteList(value);
    }
}

function addItemToDOM(text) {
  var list = document.getElementById('todo-list');

  var item = document.createElement('li'); 
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
      <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
    </span>
  </div>`;
  item.innerHTML = listItemView;

  // добавим слушатель для удаления
  var buttonDelete = item.getElementsByClassName('item-btn-del')[0];
  buttonDelete.addEventListener('click', removeItem);

  list.appendChild(item);
}

function addItemToCompleteList(text) {
  var list = document.getElementById('completed-list');

  var item = document.createElement('li');
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
  </div>`;
  item.innerHTML = listItemView;

  list.appendChild(item);
}

function removeItem(e) {
  var item = this.parentNode.parentNode.parentNode;
  var list = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;

  addItemToCompleteList(data[data.indexOf(value)]);
  completeData.push(data[data.indexOf(value)]);
  data.splice(data.indexOf(value), 1);
  
  list.removeChild(item);
  storageSync();
}

function elementFiltering() {
    if (!completeData.length) return;

    var container = [];
    var element = document.getElementById('filter').value;
    var elements = document.getElementById('completed-list').getElementsByTagName('li');

    for (var i = 0; i < completeData.length; i++) {
        var value = completeData[i];
        if(value.indexOf(element) == -1) {
            //container.push(i);
            elements[i].style.display = 'none';
        }
        else {
            elements[i].style.display = 'block';
        }
    }
    console.log(elements);
}



