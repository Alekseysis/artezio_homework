let data = [];
let buttonArray = ["7", "8", "9", "/", "C", "4", "5", "6", "*", "^2", "1", "2", "3", "-", "=", "0", ".", "+"];
let inputValue = "";
let twoValue = false;
let variable = {a:null, b:null, c:null};

for (let i=0; i < buttonArray.length; i++){
	let list = document.getElementById('calculator');
	
	let item = document.createElement('span'); 
	item.classList = 'button' + i;	
	
	let listItemView = '<div class="button">' + buttonArray[i] + '</div>';
	
	item.innerHTML = listItemView;
	list.appendChild(item);
	data.push(item);
	
	let buttonClick = item.childNodes[0];
	buttonClick.addEventListener('click', writeItem);
}

function writeItem() {
	let item = this.parentNode;
	let input = document.getElementById('input');
	let clickButton = buttonArray[data.indexOf(item)];

    if (parseInt(clickButton) || clickButton === "." || clickButton === "0"){
        if ((inputValue.indexOf('.') + 1) && clickButton === "."){
			clickButton = "";
		}
        inputValue === "0" ? inputValue = clickButton:variable.a && twoValue ? (inputValue = clickButton,twoValue = false):inputValue += clickButton;
	}
    else if (clickButton === "C"){
        inputValue = "0";
        variable.a = null;
        variable.b = null;
        variable.c = null;
    }
    else if (clickButton === "/"){
        twoValue = true;
        operationItem.call(variable);
        variable.a && variable.b ? (inputValue = String(CalcModule.div.call(variable)), variable.a = inputValue, variable.c = clickButton):console.log(clickButton);
        variable.c = clickButton;
    }
    else if (clickButton === "*"){
        twoValue = true;
        operationItem.call(variable);
        variable.a && variable.b ? (inputValue = String(CalcModule.multi(variable.a, variable.b)), variable.a = inputValue, variable.c = clickButton):console.log(clickButton);
        variable.c = clickButton;
    }
	else if (clickButton === "^2"){
        twoValue = true;
        operationItem.call(variable);
        let double = CalcModule.multi.bind(null, variable.a);
        variable.a ? (inputValue = String(double(variable.a)), variable.a = inputValue, variable.c = clickButton):console.log(clickButton);
        variable.c = clickButton;
    }
    else if (clickButton === "-"){
        twoValue = true;
        operationItem.call(variable);
        if ((variable.a && variable.b) ||  (variable.a === 0 && variable.b)){
            inputValue = String(CalcModule.sub.apply(variable));
			variable.a = inputValue;
			variable.c = clickButton;
        }
        else{
            console.log(clickButton);
        }
        variable.c = clickButton;
    }
	else if (clickButton === "+"){
        twoValue = true;
        operationItem.call(variable);
        variable.a && variable.b ? (inputValue = String(CalcModule.add.bind(variable)()), variable.a = inputValue):console.log(clickButton);
        variable.c = clickButton;
    }
	else{
		console.log(clickButton);
        operationItem.call(variable);
        if (variable.c === "/" && variable.b){
            inputValue = String(CalcModule.div.call(variable));
            twoValue = true;
        }
        else if (variable.c === "*" && variable.b){
            inputValue = String(CalcModule.multi(variable.a, variable.b));
            twoValue = true;
        }
        else if (variable.c === "^2" && variable.b){
            let double = CalcModule.multi.bind(null, variable.a);
            inputValue = String(double(variable.a));
            twoValue = true;
        }
        else if (variable.c === "-" && variable.b){console.log("!");
            inputValue = String(CalcModule.sub.apply(variable));
            twoValue = true;
        }
        else if (variable.c === "+" && variable.b){
            inputValue = String(CalcModule.add.bind(variable)());
            twoValue = true;
        }
        variable.c = clickButton;
	}
    input.value = inputValue;
}

function operationItem() {
    if (!this.b){
        if (!this.a){
            this.a = Number(inputValue);
        }
        else{
            this.b = Number(inputValue);
		}
	}
    else{
        if (this.c !== "="){
            this.b = Number(inputValue);
        }
    }console.log(variable);
}

CalcModule = 
(function(){
    let published = {};
                      
    published.add = function() {
        return this.a + this.b;
    };
                     
    published.sub = function() {
        return this.a - this.b;
    };

    published.multi = function(a, b) {
        return a * b;
    };

    published.div = function() {
        return this.a / this.b;
    };
                    
    return published;
})();
