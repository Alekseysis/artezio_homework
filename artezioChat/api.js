let apiChat = (function(){
	function getChatMessages(url, afunction) {
		axios.get(url)
			.then(function (response) {
				afunction(response.data);
			})
			.catch(function (error) {
				console.log(error);
				return [];//yield [];
			})
	}
	
	function newMessage(url, messageBody, sending){
		axios.post(url, messageBody)
			.then(function (response) {
				sending();
			})
			.catch(function (error) {
				console.log(error);
			})
	}
	
	function editMessage(url, message, edit){
		axios.put(url, message)
			.then(function () {
				edit();
			})
			.catch(function (error) {
				console.log(error);
			})
	}
	
	function removeMessage(url, remove){
		axios.delete(url)
			.then(function () {
				remove();
			})
			.catch(function (error) {
				console.log(error);
			})
	}
	
	return {
		getChatMessages: getChatMessages,
		newMessage: newMessage,
		editMessage: editMessage,
		removeMessage: removeMessage
	}
})();
