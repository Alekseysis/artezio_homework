//1
let main = (function(){
	const url = "http://localhost:8080/api/chat/message/";
	let objDiv = document.getElementById("chat-list");
	const twoSecond = 2000;
	let listMessage; 
	let apiChat;
	
	// Отображаем начальное содержимое чата
	function checkMessages(data){
		listMessage = listMessage || [];
		const messages = data;
		if(!_.isEqual(listMessage, messages)){
			listMessage = messages.slice();
			renderChatList(messages);
		}    
	}
	
	function renderChatList(messages) {
		document.getElementById('chat-message').innerHTML = '';
		const data = messages.reverse();
		data.forEach(function(message) {
			let messageText = `${message.username}: ${message.text}`;
			addItemToDOM(messageText, message.id);
		});
	}

	function addItemToDOM(text, id) {
		const tmpl = document.getElementById('message-template');
		const child = document.importNode(tmpl.content, true);
		const list = document.getElementById('chat-message');
		
		let item = child.querySelector('#collection-item');
		let temp = child.querySelector('.item-text');
		temp.textContent = text;
		
		// Добавим слушатель для кнопки вызова окна редактирования сообщения
		let buttonEdit = child.querySelector('.item-btn-edit');
		buttonEdit.addEventListener('click', function () {
			document.getElementById("editor").dataset.id = id;
			main.apiChat.getChatMessages(main.url+id, main.editWindow);
		});

		// Добавим слушатель для удаления сообщения
		let buttonDelete = child.querySelector('.item-btn-del');
		buttonDelete.addEventListener('click', function () {
			document.getElementById("delete").dataset.id = id;
			document.getElementById("submitDelete").style.display = "block";
		});

		list.appendChild(child);
	}

	//Отправить новое сообщение в чат
	document.getElementById('btn-message').addEventListener('click', function () {
		const username = document.getElementById('username').value;
		const text = document.getElementById('text').value;
		if (!text || !username) {
			alert("Поля Username и Write message должны быть заполнены!");return;
		}
		if (! isValid(username)) {
			alert("Имя не должно содержать спецсимволы!");return;
		}
		if (! isValid(text)) {
			alert("Текст сообщения не должен содержать спецсимволы!");return;
		}
		
		const messageBody = {username, text};
		main.apiChat.newMessage(main.url, messageBody, main.sendingMessage);
	});
	
	function sendingMessage(){
		document.getElementById('text').value = '';
		main.apiChat.getChatMessages(main.url, main.checkMessages);
		document.getElementById('text').focus();
		objDiv.scrollTop = objDiv.scrollHeight;
	}

	function editWindow(data) {
		if(document.getElementById("editWindow").style.display !== "none")  return;
		
		document.getElementById("editor").value = data.text;
		document.getElementById("editor").dataset.username = data.username;
		document.getElementById("editWindow").style.display = "block";
		document.getElementById('editor').focus(); 
				
		// Добавим слушатель для редактирования сообщения
		document.getElementById("btn-edit-message").addEventListener('click', editMessage);

	}
	
	function editMessage() {
		const id = document.getElementById("editor").dataset.id;
		const text = document.getElementById("editor").value;
		if (! isValid(text)) {
			alert("Текст сообщения не должен содержать спецсимволы!");return;
		}
		const username = document.getElementById("editor").dataset.username;
		const message = {id, text, username};
		
		main.apiChat.editMessage(main.url+id, message, main.closeEventEdit);
	}
	
	function closeEventEdit() {
		document.getElementById("editWindow").style.display = "none";
		document.getElementById("btn-edit-message").removeEventListener('click', editMessage);
		main.apiChat.getChatMessages(main.url, main.checkMessages);
	}

	// Добавим слушатель для закрытия окна редактирования сообщения
	document.getElementById("btn-close").addEventListener('click', function () {
		document.getElementById("editWindow").style.display = "none";
	});
	// Добавим слушатель для закрытия окна удаления сообщения
	document.getElementById("btn-delete-close").addEventListener('click', function () {
		document.getElementById("submitDelete").style.display = "none";
	});
	// Добавим слушатель для подтверждения удаления сообщения
	document.getElementById("delete-message").addEventListener('click', function () {
		id = document.getElementById("delete").dataset.id;
		main.apiChat.removeMessage(main.url+id, main.removeMessage);
	});

	function removeMessage() {
		if(document.getElementById("editWindow").style.display !== "none")  return;
		
		document.getElementById("submitDelete").style.display = "none";
		main.apiChat.getChatMessages(main.url, main.checkMessages);
	}
	
	function isValid(data) {
		if ( /^[a-zA-Z0-9.,:;?!()]+$/.test(data)) {
			return true;
		}
	}
	
	return {
		url: url,
		twoSecond: twoSecond,
		apiChat: apiChat,
		checkMessages: checkMessages,
		sendingMessage:sendingMessage,
		editWindow: editWindow,
		closeEventEdit: closeEventEdit,
		removeMessage: removeMessage
	}
	
})();

/*
if ( /^[a-zA-Z0-9.,:;?!()]+$/.test("aaa()")) {
    console.log(1);
}*/

