matrix_input = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
    [13,14,15,16]
];
matrix_output = new Array(matrix_input.length);

function transformMatrix(matrix){
    //Проверка валидности получаемых данных
    if (typeof matrix !== "object" ||
        (matrix.length === 1 && typeof matrix[0] !== "object") ||
        (matrix.length === 1 && matrix[0].length === 1)){
        console.log("Please enter the correct matrix value!");
    }
    else{
        matrixCheck(matrix);
    }
}

function matrixCheck(checkedMatrix){
    let matrixHeight = checkedMatrix.length;
    let matrixWidth = checkedMatrix[0].length;
    let result = "matrix_output = [" + '\n';

    if (matrixHeight === 1){
        //Трансформация строки
        let matrix_output = [];
        for (i = 0; i < matrixWidth; i++){
            matrix_output[i] = [];
            matrix_output[i][0] = checkedMatrix[0][i];
        }
        for (i = 0; i < matrix_output.length; i++){
            result += "[" + matrix_output[i] + "]";
            result += i < matrix_output.length -1 ? ", " + '\n':i === matrix_output.length - 1 ? "]":"";
        }
    }
    else if (matrixWidth === 1){
        //Трансформация столбца
        let matrix_output = [];
        for (i = 0; i < matrixHeight; i++){
            matrix_output[i] = checkedMatrix[i][0];
        }
        for (i = 0; i < matrix_output.length; i++){
            result += matrix_output[i];
            result += (i < matrix_output.length -1) ? ", ":i === matrix_output.length - 1 ? "]":"";
        }
    }
    else {
        //Трансформация матрицы
        let matrixComparison = 0;

        for (i = 0; i < matrixHeight; i++){
            matrixComparison += (matrixWidth === checkedMatrix[i].length) ? 1:0;
        }

        if (matrixComparison === matrixHeight){
            let outputWidth = checkedMatrix.length;
            let outputHeight = checkedMatrix[0].length;
            let matrix_output = new Array(outputHeight);

            for (i = 0; i < matrixWidth; i++){
                matrix_output[i] = new Array(outputWidth);
            }

            for (i = 0; i < matrixHeight; i++){
                for (j = 0; j < matrixWidth; j++){
                    matrix_output[j][outputWidth - 1 - i] = checkedMatrix[i][j];
                }
            }console.log(matrix_input);

            for (i = 0; i < outputHeight; i++){
                result += "[";
                for (j = 0; j < outputWidth; j++){
                    result += matrix_output[i][j];
                    result += (j !== matrix_output[0].length - 1) ? ",":"";
                }
                result += (i !== matrix_output.length -1) ? "]," + '\n':"]]";
            }
        }
    }

    console.log(result);
}

transformMatrix(matrix_input);