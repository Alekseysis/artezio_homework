/*First task*/
/*Создаем базу данных employees_information, если ее не существует*/
CREATE DATABASE IF NOT EXISTS employees_information;

/*Указываем, что используем по умолчанию базу данных employees_information*/
USE employees_information;

/*Создаем таблицу workers с полями id, first_name, last_name, post, salary*/
CREATE TABLE IF NOT EXISTS workers (
id INT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
first_name VARCHAR(30) NOT NULL,
last_name VARCHAR(30) NOT NULL,
post VARCHAR(30) NOT NULL,
salary INT NOT NULL
);

/*Добавляем новых работников*/
INSERT INTO workers ( id, first_name, last_name, post, salary) VALUES ( null, 'Ivan', 'Ivanov', 'director', 40000);
INSERT INTO workers ( id, first_name, last_name, post, salary) VALUES ( null, 'Petr', 'Petrov', 'webmaster', 20000);
INSERT INTO workers ( id, first_name, last_name, post, salary) VALUES ( null, 'Sergey', 'Sergeev', 'webmaster', 20000);
INSERT INTO workers ( id, first_name, last_name, post, salary) VALUES ( null, 'Nikolay', 'Smirnov', 'designer', 25000);
INSERT INTO workers ( id, first_name, last_name, post, salary) VALUES ( null, 'Danil', 'Sidorov', 'manager', 26000);

/*Second task*/
/*Выборка работников при зарплате меньше 30000, и при должности webmaster и зарплате меньше 30000*/
SELECT first_name, last_name FROM workers WHERE salary < 30000;
SELECT first_name, last_name, post FROM workers WHERE post = 'webmaster' AND salary < 30000;

/*Third task*/
/*Создаем таблицу для хранения отношений many-to-many subordinates с полями id, boss_id, worker_id*/
CREATE TABLE IF NOT EXISTS subordinates (
id INT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
boss_id INT UNSIGNED NOT NULL,
worker_id INT UNSIGNED NOT NULL
);

/*Добавляем связки начальник/подчиненный в таблицу subordinates*/
INSERT INTO subordinates ( id, boss_id, worker_id) VALUES ( null,
 (SELECT id FROM workers WHERE first_name = 'Ivan' AND last_name = 'Ivanov'),
 (SELECT id FROM workers WHERE first_name = 'Danil' AND last_name = 'Sidorov'));
 
INSERT INTO subordinates ( id, boss_id, worker_id) VALUES ( null,
 (SELECT id FROM workers WHERE first_name = 'Danil' AND last_name = 'Sidorov'),
 (SELECT id FROM workers WHERE first_name = 'Nikolay' AND last_name = 'Smirnov'));
 
INSERT INTO subordinates ( id, boss_id, worker_id) VALUES ( null,
 (SELECT id FROM workers WHERE first_name = 'Danil' AND last_name = 'Sidorov'),
 (SELECT id FROM workers WHERE first_name = 'Petr' AND last_name = 'Petrov'));
 
INSERT INTO subordinates ( id, boss_id, worker_id) VALUES ( null,
 (SELECT id FROM workers WHERE first_name = 'Danil' AND last_name = 'Sidorov'),
 (SELECT id FROM workers WHERE first_name = 'Sergey' AND last_name = 'Sergeev'));
 
 /*Используя значения таблицы subordinates выводим данные, с помощью INNER JOIN*/
SELECT CONCAT(w1.first_name, " ", w1.last_name) AS Boss, CONCAT(w2.first_name, " ", w2.last_name) AS Worker
FROM subordinates
INNER JOIN workers w1 ON subordinates.boss_id = w1.id
INNER JOIN workers w2 ON subordinates.worker_id = w2.id;